import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        // Declaration
        Scanner sc = new Scanner(new FileReader("input.txt")); // File for scanning
        PrintWriter wr = new PrintWriter("output.txt"); // File for output
        ReversePolishNotation my_expression = new ReversePolishNotation(); // Object of ReversePolishNotation

        // Setting for format
        DecimalFormat df = new DecimalFormat("#.##"); // Accuracy
        df.setMinimumFractionDigits(2); // Signs after decimal point
        df.setRoundingMode(RoundingMode.HALF_DOWN); // Rounding mode

        // Calculation
        try {
            wr.print(df.format(my_expression.calculate(sc.nextLine())));
        } catch (Exception e) {
            wr.print("ERROR");
        }

        // Closing of file
        wr.close();
    }
}
