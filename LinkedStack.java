/**
 * It is implementation of Linked Stack which based on Single Linked List
 * @param <T>
 */
public class LinkedStack<T> {
    // Main field - Single Linked List
    private SingleLinkedList<T> list = new SingleLinkedList<T>();

    /**
     * Push (add) element on the top of Stack return
     * @param value
     */
    public void push(T value) { list.addLast(value); }


    /**
     * Pop (remove) element from the top of Stack and return
     * @return
     */
    public T pop() { return list.removeLast(); }


    /**
     * Return top element without removing
     * @return
     */
    public T peek() { return list.getByIndex(list.getSize() - 1); }


    /**
     * Return size of Stack
     * @return
     */
    public int getSize() { return list.getSize(); }


    /**
     * Check is the Stack empty
     * @return
     */
    public boolean isEmpty() { return list.isEmpty(); }


    /**
     * Print Stack from bottom to top
     */
    public void printStack() { list.printList(); }
}
